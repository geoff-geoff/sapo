#!/usr/bin/env python3
# -*- coding: utf-8 -*-
 
from tkinter import * 

class Application(Frame): #mise en place de la frame bouton variable et lance la mise en place des combobox
    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre, width=768, height=576, **kwargs)

        self.soude = 0.0
        self.eau = 0.0
        self.les_boutons()

    def les_boutons(self):
        self.label1 = Label(fenetre, text="Olive").grid(row=0,column=0)
 
        self.v1 = StringVar()
        self.e1 = Entry(fenetre, textvariable=self.v1)
        self.e1.grid(row=0 ,column=1)
        
        self.label2 = Label(fenetre, text="Coco")
        self.label2.grid(row=1 ,column=0)
        
        self.v2 = StringVar()
        self.e2 = Entry(fenetre, textvariable=self.v2)
        self.e2.grid(row=1,column=1)
        
        self.label3 = Label(fenetre, text="Tournesol")
        self.label3.grid(row=2,column=0)

        self.v3 =StringVar()
        self.e3 =Entry(fenetre, textvariable=self.v3)
        self.e3.grid(row=2,column=1)
        
        self.label4 = Label(fenetre, text="%")
        self.label4.grid(row=3,column=0)

        self.v4 = StringVar()
        self.e4 = Entry(fenetre, textvariable=self.v4)
        self.e4.grid(row=3,column=1)



        self.button = Button(fenetre, text = "calcule", command=lambda:self.calcule(self))
        self.button.grid(row=4,column=1)



    def les_maths(self,s1=0,s2=0,s3=0,s4=1):
        self.soude = float(((0.135*s1) + (0.184*s2) + (0.136*s3))) * (s4/100)
        self.soude = self.soude - ( self.soude * (s4/100))
        
        self.eau = self.soude * 2.8
        
        self.labelsoude = Label(fenetre, text="poid de la soude:   "+str(self.soude))
        self.labelsoude.grid(row=5,column=0)

        self.labeleau = Label(fenetre, text="poid de l'eau :   "+str(self.eau))
        self.labeleau.grid(row=6,column=0)    
        
        print(self.soude, self.eau)
        
    def calcule(self,event):
        s1 = float(self.v1.get())
        s2 = float(self.v2.get())
        s3 = float(self.v3.get())
        s4 = float(self.v4.get())
        self.les_maths(s1,s2,s3,s4)
        
fenetre = Tk()
interface = Application(fenetre)

interface.mainloop()
interface.destroy()
